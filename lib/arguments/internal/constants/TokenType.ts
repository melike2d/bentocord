export enum TokenType {
	WHITESPACE,
	WORD,
	QUOTE_START,
	QUOTE_END,
	FLAG,
	OPTION,
	EOF,
}
