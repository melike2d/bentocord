export interface Parsed {
	value: string;
	key?: string;
	raw?: string;
}
